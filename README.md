# ByFileByStatement

quick script to demonstrate generators on files.

Retrieves all statements terminated with `;` within 
all files specified by directory name
and filename wildcard (real wildcard)

This code is useful within context of another system where all
statements will be batch run against an SQL database.

