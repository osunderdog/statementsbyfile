import os
import argparse
import logging
import re
from os import listdir, getcwd
from os.path import isfile, join
from itertools import chain

def parse_command_line():
    # from https://docs.python.org/3/library/argparse.html

    parser = argparse.ArgumentParser(description='Retrieve all statements from files')
    parser.add_argument('--directory', nargs="?", default=".", required=False, help="Which directory to look for files")
    parser.add_argument('--fnregex', nargs="?", default=".*\.sql", required=False,
                        help="REGEX used to identify files to examine.")
    parser.add_argument('--log', nargs="?", default="INFO", required=False, help="Logging level")
    return parser.parse_args()


def filename_gen(directory,regex):
    fnre = re.compile(regex)
    # https://stackoverflow.com/questions/3207219/how-do-i-list-all-files-of-a-directory
    return (join(directory, f) for f in listdir(directory) if isfile(join(directory, f)) and fnre.match(f))

def statement_gen(f):
    """
    Extract statements from file.  split is ";"
    Trim whitespace on right and left.  Don't return empty statements.

    :param f:
    :return:
    """
    stmntre = re.compile(";")
    with open(f, "r") as rfh:
        contents = rfh.read()
    for stmnt in stmntre.split(contents):
        #remove trailing crlf
        stmnt = str.strip(stmnt)
        if len(stmnt) == 0:
            continue
        yield stmnt

def statement_file_gen(directory, regex):
    return chain.from_iterable(statement_gen(f) for f in filename_gen(directory, regex))


def main():
    cliopts = parse_command_line()
    numeric_log_level = getattr(logging, cliopts.log.upper(), None)
    if not isinstance(numeric_log_level, int):
        raise ValueError('Invalid log level: %s' % numeric_log_level)
    logging.basicConfig(level=numeric_log_level)

    logging.info("Current Dir: [%s]", os.getcwd())
    logging.info("Directory: [%s]", cliopts.directory)
    logging.info(" FN Regex: [%s]", cliopts.fnregex)

    for idx, s in enumerate(statement_file_gen(cliopts.directory, cliopts.fnregex), 1):
        print("[{}]:[{}]".format(idx, s))


if __name__ == '__main__':
    main()
