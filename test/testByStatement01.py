import unittest
import io
import sys
from ByFileByStatement import filename_gen, statement_gen, statement_file_gen


class MyTestCase(unittest.TestCase):
    def test_directory_list(self):
        filelist = list(filename_gen("../sql","^test0[12]\.sql$"))
        self.assertEqual(2, len(filelist))

    def test_statement_list01(self):
        filename = '../sql/test01.sql'
        statementlist = list(statement_gen(filename))
        self.assertEqual(1, len(statementlist))

    def test_statement_list02(self):
        filename = '../sql/test02.sql'
        statementlist = list(statement_gen(filename))
        self.assertEqual(3, len(statementlist))

    def test_directory_statement_list(self):
        sfgen = statement_file_gen("../sql","^test0[12]\.sql$")
        filelist= list(sfgen)
        self.assertEqual(4, len(filelist))


if __name__ == '__main__':
    unittest.main()
